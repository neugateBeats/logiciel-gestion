<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController

{
    // PAGE D'ACCUIEL DE L'UTILISATEUR
    #[Route('/accueil', name: 'app_home')]
    public function index(): Response
    {
        // Vérifie si l'utilisateur connecté 
        $this->denyAccessUnlessGranted('ROLE_USER');
        
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
