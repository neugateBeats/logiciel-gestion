<?php

namespace App\Controller;

use App\Repository\TrainingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VisitorController extends AbstractController
{
    #[Route('/visiteur', name: 'app_visitor')]
    public function displayAll(TrainingRepository $trainingRepository): Response
    {
        return $this->render('visitor/index.html.twig', [
            'trainings' => $trainingRepository->findAll(),
        ]);
    }
}
