<?php

namespace App\Controller;

use App\Entity\Messaging;
use App\Entity\User;
use App\Form\MessagingType;
use App\Repository\MessagingRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessagingController extends AbstractController
{
    // AFICHER L'HISTORIQUES DES CONVERSATIONS
    #[Route('/messages', name: 'app_messaging')]
    public function index( MessagingRepository $messaging, UserRepository $userRepository): Response
    {
         // Nécessite d'avoir le rôle d'utilisateur pour accéder à cette fonction
       $this->denyAccessUnlessGranted('ROLE_USER');
       // Récupérer l'utilisateur connecté
        $user = $this->getUser();
        // Récupérer l'historique des messages envoyés par l'utilisateur
    $messaging = $userRepository->findAll($user);
        return $this->render('messaging/index.html.twig', [
            'messagings' => $messaging,
        ]);
    }


    // ENVOYER UN OU PLUSIEURS MESSAGES
    #[Route('/messages/send/{id}', name: 'app_send', methods: ['GET', 'POST'])]
    public function sendMessage( string $user, Request $request, UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $messaging = new Messaging();
        
        $form = $this->createForm(MessagingType::class, $messaging);
        $form->handleRequest($request);
        // $userRepository->findOneBy(['name'=> $user]);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $messaging->setSender($this->getUser()); // Récupérer l'utilisateur connecté comme expéditeur
            $receiverId = $form->get('receiver')->getData(); // Récupérer l'ID du destinataire à partir du formulaire
            $receiver = $userRepository->find($receiverId); // Récupérer l'entité du destinataire à partir de l'ID
            
            $messaging->setReceiver($receiver);
            $messaging->setCreatedAt(new \DateTimeImmutable());
            $messaging->setUpdateAt(new \DateTimeImmutable());
            
            $entityManager->persist($messaging);
            $entityManager->flush();
            
            // Redirection ou autre action après l'envoi du message
            
            // Exemple de redirection vers une page de confirmation
            return $this->redirectToRoute('app_messaging');
        }
        
        return $this->render('messaging/send.html.twig', [
            'MessagingTypeForm' => $form->createView(),
        ]);
    }


     // AFFICHER UNE CONVERSATION
   #[Route('messaging/showm/{id}', name: 'app_showm', methods: ['GET'])]
   public function show(MessagingRepository $messaging, int $id): Response
   {

    // Vérifie si l'utilisateur connecté 
    $this->denyAccessUnlessGranted('ROLE_USER');

     // Affiche la vue
     return $this->render('messaging/showm.html.twig', [
        'messaging' => $messaging->find($id),
        
    ]);

   }
}
