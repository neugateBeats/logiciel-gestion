<?php

namespace App\Controller;

use App\Entity\Consultant;
use App\Entity\Training;
use App\Repository\ConsultantRepository;
use App\Repository\TrainingRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class TrainingController extends AbstractController
{
   // AFFICHE LES FORMATIONS
   #[Route('/formations', name: 'app_training', methods: ['GET'])]
   public function displayAll(TrainingRepository $trainingRepository, Environment $twig): Response
   {
       // Nécessite d'avoir le rôle d'utilisateur pour accéder à cette fonction
       $this->denyAccessUnlessGranted('ROLE_USER');

        // Vérifier si l'utilisateur a le rôle "ROLE_CONSULTANT" ou "ROLE_CRC"
        if ($this->isGranted('ROLE_CONSULTANT') || $this->isGranted('ROLE_CRC')) {
            throw $this->createAccessDeniedException();
        }

        // Récuperer  les formations
        $trainings = $trainingRepository->findAll();

        // Échapper les données avant de les passer à la vue
        foreach ($trainings as &$training) {

            $training->setName($twig->getFilter('escape')->getCallable()($twig, $training->getName()));
            // Échapper d'autres propriétés si nécessaire
        }

       // Affiche la vue
       return $this->render('training/index.html.twig', [
          'trainings' => $trainings,
       ]);
   }

   // AFFICHE UNE FORMATION AVEC UN OU PLUSIEURS CONSULTANTS SUR LA MAP
   #[Route('formations/show/{id}', name: 'app_show', methods: ['GET'])]
   public function show(Training $training, UserRepository $user): Response
   {

       // Nécessite d'avoir le rôle d'utilisateur pour accéder à cette fonction
       $this->denyAccessUnlessGranted('ROLE_USER');

        // Vérifier si l'utilisateur a le rôle "ROLE_CONSULTANT" ou "ROLE_CRC"
        if ($this->isGranted('ROLE_CONSULTANT') || $this->isGranted('ROLE_CRC')) {
            throw $this->createAccessDeniedException();
        }

       if (!$training) {
        throw $this->createNotFoundException('Formation non trouvée');
    }
        // Récupère les consultants associés à la formation
        $users = $user->findBy(['training' => $training]);
        
        // Créer un tableau vide pour stocker les données des consultants
        $usersData = [];

        // Parcours la liste des consultants et récupère les informations nécessaires
        foreach ($users as $user) {
            $usersData[] = [
                'name' => $user->getName(),
                'firstname' => $user->getFirstname(),
                'city' => $user->getCity(),
                'latitude' => $user->getLatitude(),
                'longitude' => $user->getLongitude(),
            ];
        }

       // Affiche la vue
       return $this->render('training/show.html.twig', [
           'training' => $training,
           'users' => $users,
        // Encode les données des consultants au format JSON
           'usersData' => json_encode($usersData)
           
       ]);
   }

}
