<?php

namespace App\Controller;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

class UserController extends AbstractController
{
    //AFFICHAGE DE LA PAGE DE PROFIL
    #[Route('/utilisateur', name: 'app_user')]
    public function index(): Response
    {
        // Vérifie si l'utilisateur connecté 
        $this->denyAccessUnlessGranted('ROLE_USER');

        // Afficher la vue 
        return $this->render('user/index.html.twig');
    }
    
    // MODIFIER LE PROFIL DE L'UTILISATEUR
    #[Route('/user/editprofil', name: 'app_editprofil', methods: ['GET', 'POST'])]
    public function eidtProfil(Request $request, UserRepository $userRepository): Response
    {
        // Vérifie si l'utilisateur connecté 
        $this->denyAccessUnlessGranted('ROLE_USER');

        // Récupère l'utilisateur connecté
         /** @var User $user */ 
         $user = $this->getUser();

         // Créer le formulaire
         $form = $this->createFormBuilder($user)
         ->add('name', TextType::class)
         ->add('firstname', TextType::class)
         ->add('address', TextType::class)
         ->add('postalcode', TextType::class)
         ->add('city', TextType::class)
         ->add('phone', NumberType::class)
         ->add('datebirth', DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
         ])
         ->add('email', TextType::class)
         ->add('picture', FileType::class, [
             'mapped' => false,
             'required' => false,
             'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/png', 'image/jpeg', 'image/jpg'
                    ],
                    'mimeTypesMessage' => 'Vous ne pouvez enregistrer que les formats .png, .jpg ou .jpeg.',
                    'maxSize' => '2000k'
                ])
            ]
         ])
         // Ajout d'un bouton "Valider" de type "submit"
         ->add('Modification', SubmitType::class)

         ->getForm();

        // Associe le formulaire à la requête HTTP pour récupérer les données envoyées par l'utilisateur
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            // Vérifie si une image a été téléchargée dans le formulaire
            $picture = $form->get('picture')->getData();

            // Si une image a été téléchargée, on la traite et on la déplace dans le répertoire de destination
            if ($picture) {
                $fileName = md5(uniqid(rand())) . "." . $picture->guessExtension();
                $fileDestination = $this->getParameter('profile_pictures_dir');

             // Si une erreur se produit lors du déplacement de l'image, on lance une exception HTTP 500   
                try {
                    $picture->move($fileDestination, $fileName);
                } catch (FileException $e) {
                    throw new HttpException(500, 'Erreur');
                }


            } else {
                // Si aucune image n'a été téléchargée, on utilise l'image par défaut
                $fileName = 'profile.png';
            }
            
            // On met à jour le nom de l'image de profil de l'utilisateur
            $user->setPicture($fileName);

            // On enregistre les modifications dans la base de données
            $userRepository->save($user, true);

            // On affiche un message de confirmation à l'utilisateur
            $this->addFlash('message', 'profil mis à jour');

            // On redirige l'utilisateur vers la page profil
            return $this->redirectToRoute('app_user');
        }

        // Si le formulaire n'a pas été soumis ou s'il est invalide, on affiche la vue du formulaire de modification de profil
        return $this->render('user/editprofil.html.twig', [

            // On envoie le formulaire à la vue pour qu'il puisse être affiché
            'editProfileForm' =>$form->createView(),
        ]);
    }

    // MODIFIER LE MOT DE PASSE
    #[Route('/user/editpass/', name: 'app_editpass', methods: ['GET', 'POST'])]
    public function editPass(Request $request, EntityManagerInterface $manager) : Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        /** @var User $user */ 
        $user = $this->getUser();

        // Creation du formulaire pour la modification du mot de passe de l'utilisateur
        $form = $this->createFormBuilder()
        ->add('oldPassword', PasswordType::class)
        ->add('newPassword', PasswordType::class)
        ->add('confirmPassword', PasswordType::class)

        //Ajout d'un bouton pour soumettre le formulaire
        ->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-primary mt-4'
            ],
            'label' => 'Changer'
        ])
        ->getForm();

        $form->handleRequest($request);

        // Si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupère les données du formulaire
            $oldPassword = $form->get('oldPassword')->getData();
            $newPassword = $form->get('newPassword')->getData();
            $confirmPassword = $form->get('confirmPassword')->getData();

            // Vérifie si les deux champs de nouveau mot de passe correspondent
            if ($newPassword !== $confirmPassword) {
                // Affiche un message d'erreur et redirige vers la page de modification de mot de passe
                $this->addFlash('danger', 'Les mots de passe ne correspondent pas');
                return $this->redirectToRoute('app_editpass');
            }

             // Vérifie si l'ancien mot de passe est correct
            if (!password_verify($oldPassword, $user->getPassword())) {
                // Affiche un message d'erreur et redirige vers la page de modification de mot de passe
                $this->addFlash('danger', 'L\'ancien mot de passe est incorrect');
                return $this->redirectToRoute('app_editpass');
            }
            // Hashe le nouveau mot de passe
            $hashedPassword = password_hash($newPassword, PASSWORD_BCRYPT);
            $user->setPassword($hashedPassword);

            // Sauvegarde de l'utilisateur
            $manager->persist($user);
            $manager->flush();

             // Redirection vers la page de profil de l'utilisateur
            $this->addFlash('success', 'Mot de passe modifié avec succès');
            return $this->redirectToRoute('app_user');

        }

             // Affichage du formulaire
            return $this->render('user/editpass.html.twig', [
            'editPassForm' => $form->createView()
      ]);


    }

}
