<?php

namespace App\Controller;

use App\Entity\Consultant;
use App\Entity\Meeting;
use App\Entity\User;
use App\Entity\Training;
use App\Repository\ConsultantRepository;
use App\Repository\MeetingRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security as SecurityBundleSecurity;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MeetingController extends AbstractController
{

// CREER UN RENDEZ-VOUS
#[Route('/meeting/create/{id}', name: 'app_create', methods: ['GET', 'POST'])]
public function new(Request $request,$id, MeetingRepository $meeting, EntityManagerInterface $manager): Response
{
    $this->denyAccessUnlessGranted('ROLE_USER');

    // Récupération de l'ID du consultant depuis la requête
// $consultantId = $request->query->get('id');

// Récupération du consultant à partir de l'ID
$consultant = $manager->getRepository(User::class)->find($id);

// Récupération de la première formation disponible
$training = $manager->getRepository(Training::class)->findOneBy([]);

     // Creation du formulaire
     $form = $this->createFormBuilder()
     ->add('start_date', DateTimeType::class, [
        'widget' => 'single_text',
        'html5' => true,
        'attr' => [
            'type' => 'datetime-local',
        ],
        'hours' => [9, 12, 15, 18], // heures acceptables
     ])
     ->add('end_date', DateTimeType::class, [
        'widget' => 'single_text',
        'html5' => true,
        'attr' => [
            'type' => 'datetime-local',
        ],
        'hours' => [9, 12, 15, 18], // heures acceptables
     ])
     ->add('description', TextType::class)

     ->add('training_id', EntityType::class, [
        'class' => Training::class,
        'label'=> 'Formation',
        'choice_label' => 'name',
        'data' => $training, // Formation pré-remplie
    ])
    ->add('receiver_id', EntityType::class, [
        'class' => User::class,
        'label'=> 'Consultant',
        'choice_label' => 'name',
        'data' => $consultant, // Consultant pré-rempli
    ])

     //Ajout d'un bouton pour soumettre le formulaire
     ->add('submit', SubmitType::class, [
         'attr' => [
             'class' => 'btn btn-primary mt-4'
         ],
         'label' => 'Valider'
     ])
     ->getForm();
     $form->handleRequest($request);

     if ($form->isSubmitted() && $form->isValid()) {

         // Récupération des données du formulaire
        $data = $form->getData();

        // Création d'un nouvel objet Meeting avec les données du formulaire
        $meeting = new Meeting();
        $meeting->setStartDate($data['start_date']);
        $meeting->setEndDate($data['end_date']);
        $meeting->setDescription($data['description']);

        // Récupération de l'utilisateur connecté
            $user = $this->getUser();
            $meeting->setSender($user);

        // Récupération de la formation sélectionnée dans le formulaire
        $trainingId = $data['training_id'];
        $training = $manager->getRepository(Training::class)->find($trainingId);
        $meeting->setTraining($training);

        // Récupération du consultant sélectionné dans le formulaire
        $consultantId = $data['receiver_id'];
        $consultant = $manager->getRepository(User::class)->find($consultantId);
        $meeting->setReceiver($consultant);
        $manager->persist($meeting);
        $manager->flush();

        // Redirection vers la page d'accueil
        return $this->redirectToRoute('app_meeting');   
     }
    
     // Affichage du formulaire
    return $this->render('meeting/create.html.twig', [
        'createForm' => $form->createView(),
    ]);
}



    // AFFICHER LES RENDEZ-VOUS D'UN UTILISATEUR CONNECTE
    #[Route('/Rendez-vous', name: 'app_meeting')]
public function index(MeetingRepository $meeting): Response
{
    $this->denyAccessUnlessGranted('ROLE_USER');
    $user = $this->getUser();

     // Récupérer les rendez-vous de l'utilisateur connecté en tant qu'émetteur (sender) ou destinataire (receiver)
     $events = $meeting->createQueryBuilder('m')
     ->andWhere('m.sender = :user OR m.receiver = :user')
     ->setParameter('user', $user)
     ->getQuery()
     ->getResult();

       // Créer un tableau vide pour les rendez-vous
    $rdvs = [];

    // Ajouter les rendez-vous de l'utilisateur connecté en tant que emetteur (sender)
    foreach ($events as $event) {
        $rdvs[] = [
            'id' => $event->getId(),
            'start' => $event->getStartDate()->format('Y-m-d H:i:s'),
            'end' => $event->getEndDate()->format('Y-m-d H:i:s'),
            'title' => $event->getDescription(),
            'description' => $event->getDescription(),
            
        ];
    }
    // Si l'utilisateur n'a pas de rendez-vous, ajouter un rendez-vous vide au tableau
    if (empty($rdvs)) {
        $rdvs[] = [
            'id' => '0',
            'start' => '',
            'end' => '',
            'title' => 'Aucun rendez-vous',
            'description' => '',
        ];
    }
    $data = json_encode($rdvs);
    return $this->render('meeting/index.html.twig', compact('data'));
}

 // AFFICHER LES RENDEZ-VOUS D'UN CONSULTANT
#[Route('/consultant/{id}/rendez-vous', name: 'app_show_receiver_meetings')]
    public function showReceiverMeetings(MeetingRepository $meetingRepository, UserRepository $userRepository, $id): Response
    {
        $receiver = $userRepository->find($id);
        if (!$receiver) {
            throw $this->createNotFoundException('Consultant non trouvé');
        }

        $receiverMeetings = $meetingRepository->findBy(['receiver' => $receiver]);

        // Ajouter les rendez-vous du receiver dans un tableau
        $rdvs = [];
        foreach ($receiverMeetings as $meeting) {
            $rdvs[] = [
                'id' => $meeting->getId(),
                'start' => $meeting->getStartDate()->format('Y-m-d H:i:s'),
                'end' => $meeting->getEndDate()->format('Y-m-d H:i:s'),
                'title' => $meeting->getDescription(),
                'description' => $meeting->getDescription(),
            
            ];
        }

        // Si le receiver n'a pas de rendez-vous, ajouter un rendez-vous vide au tableau
        if (empty($rdvs)) {
            $rdvs[] = [
                'id' => '0',
                'start' => '',
                'end' => '',
                'title' => 'Aucun rendez-vous',
                'description' => '',
                'emptyEvent' => true, // Ajouter la propriété emptyEvent,
            ];
        }

        $data = json_encode($rdvs);

        return $this->render('meeting/receiver_meetings.html.twig', compact('data'));
    }
    
}
