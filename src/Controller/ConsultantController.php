<?php

namespace App\Controller;

use App\Entity\Consultant;
use App\Entity\Training;
use App\Repository\ConsultantRepository;
use App\Repository\TrainingRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as ConfigurationSecurity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConsultantController extends AbstractController
{

    // AFFICHE UNE UN CONSULTANT
   #[Route('consultant/show/{id}', name: 'show', methods: ['GET'])]
   public function show(UserRepository $user, TrainingRepository $training, int $id): Response
   {
       // Nécessite d'avoir le rôle d'utilisateur pour accéder à cette fonction
       $this->denyAccessUnlessGranted('ROLE_USER');

    //    // Récupération de l'utilisateur connecté
    // $user = $this->getUser();

    // // Vérification du rôle de l'utilisateur connecté
    // if ($user && in_array('ROLE_USER', $user->getRoles(), true)) {
    //     // Récupération du consultant associé à un utilisateur ayant le rôle ROLE_CONSULTANT
    //     $consul = $consultant->findAssociatedConsultantByRole($id, 'ROLE_CONSULTANT');
        
    //     if (!$consul) {
    //         throw $this->createNotFoundException('Consultant non trouvé.');
    //     }
    

       // Affiche la vue
       return $this->render('consultant/show.html.twig', [
           'consultant' => $user->find($id),
        //    'trainingName' => $trainingName,
           
       ]);
//    }
//    throw $this->createAccessDeniedException('Vous n\'avez pas la permission d\'accéder à cette page.');
}

}


