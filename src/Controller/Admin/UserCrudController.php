<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('reference'),
            TextField::new('name'),
            TextField::new('firstname'),
            DateField::new('datebirth'),
            ArrayField::new('roles'),
            TextField::new('address'),
            IntegerField::new('postalcode'),
            TextField::new('city'),
            IntegerField::new('phone'),
            TextField::new('email'),
            TextField::new('password')->onlyWhenCreating(),
            ImageField::new('picture')->setUploadDir('public/profile/'),
            BooleanField::new('is_verified'),
        ];
    }
    
}
