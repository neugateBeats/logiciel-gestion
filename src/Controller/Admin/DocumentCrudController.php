<?php

namespace App\Controller\Admin;

use App\Entity\Document;
use Doctrine\DBAL\Types\BlobType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use EasyCorp\Bundle\EasyAdminBundle\Field\FileField;

class DocumentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Document::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        
       
        return [
            IdField::new('id')
            ->hideOnForm(),
            TextField::new('name'),
            TextField::new('format'),
            TextField::new('size'),
            Field::new('data')->setFormTypeOptions([
                'mapped' => false, // ne pas enregistrer le fichier sur le disque
            'required' => false, // autoriser l'utilisateur à ne pas télécharger de fichier
            'attr' => ['accept' => '.doc,.docx,.pdf'], // limiter les types de fichiers autorisés
            ])
            

        ];
    }
    
}
