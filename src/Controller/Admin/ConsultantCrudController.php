<?php

namespace App\Controller\Admin;

use App\Entity\Consultant;
use App\Entity\Training;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ConsultantCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Consultant::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
            ->hideOnForm(),
        TextField::new('name'),
        TextField::new('firstname'),
        TextField::new('address'),
        IntegerField::new('postalcode'),
        TextField::new('city'),
        EmailField::new('email'),
        IntegerField::new('phone'),
        NumberField::new('latitude'),
        NumberField::new('longitude'),
        AssociationField::new('training')
            ->setFormType(TrainingType::class)
            ->setFormTypeOptions([
                'class' => Training::class,
                'choice_label' => 'name'
            ]),
        

        ];
    }
    
}
