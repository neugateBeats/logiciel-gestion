<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RoleConsultantController extends AbstractController
{
    #[Route('/role/consultant', name: 'app_role_consultant')]
    public function index(): Response
    {
        return $this->render('role_consultant/index.html.twig', [
            'controller_name' => 'RoleConsultantController',
        ]);
    }
}
