<?php

namespace App\Controller;

use App\Entity\Document;
use App\Form\DocumentFormType;
use App\Repository\DocumentRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class DocumentController extends AbstractController
{
    // AFFICHER LES DOCUMENTS
    #[Route('/document', name: 'app_document', methods: ['GET'])]
    public function index(DocumentRepository $document) {

     // Nécessite d'avoir le rôle d'utilisateur pour accéder à cette fonction
       $this->denyAccessUnlessGranted('ROLE_USER');

        // Vérifier si l'utilisateur a le rôle "ROLE_CONSULTANT"
        if ($this->isGranted('ROLE_CONSULTANT')) {
            throw $this->createAccessDeniedException();
        }

       return $this->render('document/index.html.twig', [
        'documents' => $document->findAll(),
       ]);

    
    }

    // AFFICHER UN DOCUMENT ET TELECHARGER
    #[Route("/uploads/documents/{fileName}", name: 'view_document')]
    public function view(string $fileName)
    {
        // Vérification de la validité du nom de fichier
        $filePath = $this->getParameter('documents_directory').'/'.$fileName;
  
     // Vérification du type de fichier autorisé
     $allowedExtensions = ['pdf', 'doc', 'docx'];
     $extension = pathinfo($filePath, PATHINFO_EXTENSION);

     // Comparaison de l'extension avec les extensions autorisées
     if (!in_array($extension, $allowedExtensions)) {
        
        // Si l'extension n'est pas autorisée, une exception de type 'AccessDeniedException' est levée
         throw $this->createAccessDeniedException('Type de fichier non autorisé');
     }

        return $this->file($filePath);
    }
    

    //  FOMULAIRE POUR ENREGISTRER LES DOCUMENTS
    #[Route('/document/form', name: 'app_form', methods: ['GET', 'POST'])]
    public function uploadDoc(DocumentRepository $document, Request $request, SluggerInterface $slugger, EntityManagerInterface $manager): Response
    {
         // Nécessite d'avoir le rôle d'utilisateur pour accéder à cette fonction
       $this->denyAccessUnlessGranted('ROLE_USER');
       
        // Vérifier si l'utilisateur a le rôle "ROLE_CONSULTANT"
        if ($this->isGranted('ROLE_CONSULTANT')) {
            throw $this->createAccessDeniedException();
        }
    
       // Crée une nouvelle instance de Document et récupère le formulaire correspondant
         $document = new Document();
         $form = $this->createForm(DocumentFormType::class, $document);
         $form->handleRequest($request);
    
         // Si le formulaire est soumis et valide, traite les données envoyées
         if ($form->isSubmitted() && $form->isValid()) {
            
              // Récupère le fichier de données
              $dataFile = $form->get('data')->getData();
              if ($dataFile) {
                // Obtient le nom de fichier original et le sécurise
                $originalFilename = pathinfo($dataFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                // Crée un nouveau nom de fichier unique
                $newFilename = $safeFilename.'-'.uniqid().'.'.$dataFile->guessExtension();
    
                try {
                    // Déplace le fichier vers le répertoire de stockage de documents
                    $dataFile->move(
                        $this->getParameter('documents_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // Gère l'exception si le fichier ne peut pas être déplacé
                }
    
                // Met à jour l'entité Document avec le nom du fichier nouvellement créé
                $document->setData($newFilename);
            }
    
        // Enregistre l'entité dans la base de données
        $manager->persist($document);
        $manager->flush();
    
        // Redirige vers la liste des documents
        return $this->redirectToRoute('app_document');
        
         }
      
        // Affiche le formulaire
        return $this->render('document/form.html.twig', [
                     'docForm' => $form->createView(),
                     'document' => $document,
                    ]);
        
    }
    
    
}
