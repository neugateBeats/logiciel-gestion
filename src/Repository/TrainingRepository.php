<?php

namespace App\Repository;

use App\Entity\Training;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Training>
 *
 * @method Training|null find($id, $lockMode = null, $lockVersion = null)
 * @method Training|null findOneBy(array $criteria, array $orderBy = null)
 * @method Training[]    findAll()
 * @method Training[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrainingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        // Appelle le constructeur de la classe parente (ServiceEntityRepository) avec les paramètres $registry et la classe Training
        parent::__construct($registry, Training::class);
    }

    // Enregistre une entité Training
    public function save(Training $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        // Effectue une sauvegarde en base de données si le paramètre $flush est vrai
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

     // Supprime une entité Training
    public function remove(Training $entity, bool $flush = false): void
{
        $this->getEntityManager()->remove($entity);

         // Effectue une sauvegarde en base de données si le paramètre $flush est vrai
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


}
