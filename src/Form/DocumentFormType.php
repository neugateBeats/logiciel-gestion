<?php

namespace App\Form;

use App\Entity\Document;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
 use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class DocumentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
{
    $builder
        // Ajouter un champ "name" de type TextType
        ->add('name', TextType::class )
        // Ajouter un champ "format" de type TextType
        ->add('format', TextType::class)
        // Ajouter un champ "size" de type TextType
        ->add('size', texttype::class)
        // Ajouter un champ "data" de type FileType
        ->add('data',FileType::class, [
            'label' => 'fichier (PDF file)', // Libellé du champ
            'mapped' => false, // Ne pas mapper ce champ sur une propriété de l'entité
            'required' => false, // Le champ n'est pas obligatoire
            'constraints' => [
                new File([
                    'maxSize' => '5120k', // Taille maximale du fichier autorisée (en kilo-octets)
                    'mimeTypes' => [
                        'application/pdf', // Types MIME autorisés
                        'application/x-pdf',
                        'application/msword',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    ],
                    'mimeTypesMessage' => 'Veuillez télécharger un document PDF valide', // Message d'erreur si le fichier ne correspond pas aux types MIME autorisés
                ])
            ],
        ])
        // Ajouter un bouton de soumission de type SubmitType
        ->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-primary mt-4' // Classe CSS appliquée au bouton de soumission
            ],
            'label' => 'Valider' // Libellé du bouton de soumission
        ]);
}

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
