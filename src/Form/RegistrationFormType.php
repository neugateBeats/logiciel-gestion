<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('name',TextType::class,[
            'label' => 'form.contact.name.label',
                'attr' => [
                    'placeholder' => 'LeGrand'
                ],
        ])
        ->add('firstname', TextType::class,[
            'label' => 'form.contact.name.label',
                'attr' => [
                    'placeholder' => 'Paul'
                ],
        ])
        ->add('reference', TextType::class,[
            'label' => 'form.contact.name.label',
                'attr' => [
                    'placeholder' => 'ID'
                ],
        ])
        ->add('datebirth', DateType::class, [
            'widget' => 'single_text',
            
            'format' => 'yyyy-MM-dd',
        ])
        ->add('address', TextType::class,[
            'label' => 'form.contact.name.label',
                'attr' => [
                    'placeholder' => 'adresse'
                ],
        ])
        ->add('city', TextType::class,[
            'label' => 'form.contact.name.label',
                'attr' => [
                    'placeholder' => 'Ville'
                ],
        ])
        ->add('postalcode', IntegerType::class,[
            'label' => 'form.contact.name.label',
                'attr' => [
                    'placeholder' => 'Code Postal'
                ],
        ])
        ->add('phone', IntegerType::class,[
            'label' => 'form.contact.name.label',
                'attr' => [
                    'placeholder' => 'Télephone'
                ],
        ])
        ->add('email', EmailType::class,[
            'label' => 'form.contact.name.label',
                'attr' => [
                    'placeholder' => 'Adresse Mail'
                ],
        ])
        ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter nos termes.',
                    ]),
                ],
            ])
            // ->add('picture', FileType::class, [
            //     'label' => 'Profile Picture',
            //     'mapped' => false,
            //     'required' => false,
            //     'constraints' => [
            //     ],
            // ])
            ->add('plainPassword', RepeatedType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                    'label' => 'form.contact.name.label',
                        'attr' => [
                            'placeholder' => 'Mots de passe'
                        ],
                'type' => PasswordType::class,
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 8,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 64,
                    ]),
                    new Regex([
                        'pattern' =>  '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/',
                        'message' => 'Le mot de passe doit contenir au moins une lettre majuscule, une lettre minuscule, un chiffre et un symbole.',
                        ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
