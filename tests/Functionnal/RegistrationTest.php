<?php

namespace App\Tests\Functionnal;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class RegistrationTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
    $crawler = $client->request('GET', '/inscription');

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('h1', 'Inscription');

    // Vérifier que le formulaire existe
    $form = $crawler->filter('form[name="inscription"]')->form();
    $this->assertNotNull($form, 'Le formulaire est introuvable');

        $form["inscription[name]"] = "Jean";
        $form["inscription[firstname]"] = "Dupont";
        $form["inscription[reference]"] = "1234";
        $form["inscription[datebirth]"] = "12/12/1987";
        $form["inscription[address]"] = "1 allée michel";
        $form["inscription[city]"] = "JACOU";
        $form["inscription[postalcode]"] = "34830";
        $form["inscription[phone]"] = "0601275571";
        $form["inscription[email]"] = "jd@symrecipe.com";
       
        // soumettere le formulaire 
        $client->submit($form);

        // Vérifier le statut HTTP
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);

        // Vérifier la présence du message de succès
        $this->assertSelectorTextContains(
            'div.alert.alert-success.mt-4',
            "Votre demande s'inscription a été envoyé avec succès !"
        );


    }
}
