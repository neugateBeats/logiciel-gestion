<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Training;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TrainingTest extends KernelTestCase
{
    public function testGettersAndSetters(): void
    {
        self::bootKernel();

        $container = static::getContainer();

// Création d'une instance de l'entité Training pour les tests
        $training = new Training();
        $name = 'VAE';
        $description = 'formation initial';
// Utilisation des setters pour définir les valeurs des propriétés du training
        $training->setName($name);
        $training->setDescription($description);

        // Vérifie que le nom défini est identique au nom récupéré
        $this->assertSame($name, $training->getName());
        // Vérifie que la description définie est identique à la description récupérée
        $this->assertSame($description, $training->getDescription());
        

    }
}
