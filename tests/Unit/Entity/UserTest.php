<?php

namespace App\Tests\Unit\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase
{
    public function testGettersAndSetters(): void
    {
        self::bootKernel();

       $container = static::getContainer();

    // Création d'une instance de l'entité User pour les tests
        $user = new User();
        $name = 'John Doe';
        $firstname = 'michel';
        $email = 'john@example.com';
        $roles = ['ROLE_USER'];
        $password = 'password123';

        // Utilisation des setters pour définir les valeurs des propriétés de l'utilisateur
        $user->setName($name);
        $user->setFirstname($firstname);
        $user->setEmail($email);
        $user->setRoles($roles);
        $user->setPassword($password);
        

        // Assertions pour vérifier que les getters retournent les valeurs correctes
        $this->assertEquals($name, $user->getName());
        $this->assertEquals($firstname, $user->getFirstname());
        $this->assertEquals($email, $user->getEmail());
        $this->assertEquals($roles, $user->getRoles());
        $this->assertEquals($password, $user->getPassword());

        // Ajoutez ici des assertions supplémentaires pour tester les autres getters et setters de l'entité User
    }
}
