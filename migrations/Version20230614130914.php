<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230614130914 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE messaging DROP FOREIGN KEY FK_EE15BA61A76ED395');
        $this->addSql('DROP INDEX IDX_EE15BA61A76ED395 ON messaging');
        $this->addSql('ALTER TABLE messaging ADD receiver_id INT DEFAULT NULL, ADD update_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE user_id sender_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE messaging ADD CONSTRAINT FK_EE15BA61F624B39D FOREIGN KEY (sender_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE messaging ADD CONSTRAINT FK_EE15BA61CD53EDB6 FOREIGN KEY (receiver_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_EE15BA61F624B39D ON messaging (sender_id)');
        $this->addSql('CREATE INDEX IDX_EE15BA61CD53EDB6 ON messaging (receiver_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE messaging DROP FOREIGN KEY FK_EE15BA61F624B39D');
        $this->addSql('ALTER TABLE messaging DROP FOREIGN KEY FK_EE15BA61CD53EDB6');
        $this->addSql('DROP INDEX IDX_EE15BA61F624B39D ON messaging');
        $this->addSql('DROP INDEX IDX_EE15BA61CD53EDB6 ON messaging');
        $this->addSql('ALTER TABLE messaging ADD user_id INT DEFAULT NULL, DROP sender_id, DROP receiver_id, DROP update_at');
        $this->addSql('ALTER TABLE messaging ADD CONSTRAINT FK_EE15BA61A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_EE15BA61A76ED395 ON messaging (user_id)');
    }
}
