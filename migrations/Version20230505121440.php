<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230505121440 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD crc_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649C232B37 FOREIGN KEY (crc_id) REFERENCES crc (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649C232B37 ON user (crc_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649C232B37');
        $this->addSql('DROP INDEX UNIQ_8D93D649C232B37 ON user');
        $this->addSql('ALTER TABLE user DROP crc_id');
    }
}
