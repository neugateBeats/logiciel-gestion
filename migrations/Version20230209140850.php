<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230209140850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE document_training (document_id INT NOT NULL, training_id INT NOT NULL, INDEX IDX_5AD37BD6C33F7837 (document_id), INDEX IDX_5AD37BD6BEFD98D1 (training_id), PRIMARY KEY(document_id, training_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE document_training ADD CONSTRAINT FK_5AD37BD6C33F7837 FOREIGN KEY (document_id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document_training ADD CONSTRAINT FK_5AD37BD6BEFD98D1 FOREIGN KEY (training_id) REFERENCES training (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE document_training DROP FOREIGN KEY FK_5AD37BD6C33F7837');
        $this->addSql('ALTER TABLE document_training DROP FOREIGN KEY FK_5AD37BD6BEFD98D1');
        $this->addSql('DROP TABLE document_training');
    }
}
