<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230612095418 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE consultant DROP FOREIGN KEY FK_441282A1A76ED395');
        $this->addSql('ALTER TABLE consultant DROP FOREIGN KEY FK_441282A1BEFD98D1');
        $this->addSql('DROP INDEX IDX_441282A1A76ED395 ON consultant');
        $this->addSql('DROP INDEX IDX_441282A1BEFD98D1 ON consultant');
        $this->addSql('ALTER TABLE consultant DROP training_id, DROP user_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE consultant ADD training_id INT DEFAULT NULL, ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE consultant ADD CONSTRAINT FK_441282A1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE consultant ADD CONSTRAINT FK_441282A1BEFD98D1 FOREIGN KEY (training_id) REFERENCES training (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_441282A1A76ED395 ON consultant (user_id)');
        $this->addSql('CREATE INDEX IDX_441282A1BEFD98D1 ON consultant (training_id)');
    }
}
