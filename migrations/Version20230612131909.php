<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230612131909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meeting ADD sender_id INT DEFAULT NULL, ADD receiver_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE meeting ADD CONSTRAINT FK_F515E139F624B39D FOREIGN KEY (sender_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE meeting ADD CONSTRAINT FK_F515E139CD53EDB6 FOREIGN KEY (receiver_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_F515E139F624B39D ON meeting (sender_id)');
        $this->addSql('CREATE INDEX IDX_F515E139CD53EDB6 ON meeting (receiver_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meeting DROP FOREIGN KEY FK_F515E139F624B39D');
        $this->addSql('ALTER TABLE meeting DROP FOREIGN KEY FK_F515E139CD53EDB6');
        $this->addSql('DROP INDEX IDX_F515E139F624B39D ON meeting');
        $this->addSql('DROP INDEX IDX_F515E139CD53EDB6 ON meeting');
        $this->addSql('ALTER TABLE meeting DROP sender_id, DROP receiver_id');
    }
}
