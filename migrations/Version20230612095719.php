<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230612095719 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meeting DROP FOREIGN KEY FK_F515E13944F779A2');
        $this->addSql('DROP INDEX IDX_F515E13944F779A2 ON meeting');
        $this->addSql('ALTER TABLE meeting DROP consultant_id');
        $this->addSql('ALTER TABLE messaging DROP FOREIGN KEY FK_EE15BA6144F779A2');
        $this->addSql('DROP INDEX IDX_EE15BA6144F779A2 ON messaging');
        $this->addSql('ALTER TABLE messaging DROP consultant_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE messaging ADD consultant_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE messaging ADD CONSTRAINT FK_EE15BA6144F779A2 FOREIGN KEY (consultant_id) REFERENCES consultant (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_EE15BA6144F779A2 ON messaging (consultant_id)');
        $this->addSql('ALTER TABLE meeting ADD consultant_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE meeting ADD CONSTRAINT FK_F515E13944F779A2 FOREIGN KEY (consultant_id) REFERENCES consultant (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_F515E13944F779A2 ON meeting (consultant_id)');
    }
}
