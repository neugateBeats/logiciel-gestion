<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230209140450 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meeting ADD user_id INT DEFAULT NULL, ADD training_id INT DEFAULT NULL, ADD consultant_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE meeting ADD CONSTRAINT FK_F515E139A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE meeting ADD CONSTRAINT FK_F515E139BEFD98D1 FOREIGN KEY (training_id) REFERENCES training (id)');
        $this->addSql('ALTER TABLE meeting ADD CONSTRAINT FK_F515E13944F779A2 FOREIGN KEY (consultant_id) REFERENCES consultant (id)');
        $this->addSql('CREATE INDEX IDX_F515E139A76ED395 ON meeting (user_id)');
        $this->addSql('CREATE INDEX IDX_F515E139BEFD98D1 ON meeting (training_id)');
        $this->addSql('CREATE INDEX IDX_F515E13944F779A2 ON meeting (consultant_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meeting DROP FOREIGN KEY FK_F515E139A76ED395');
        $this->addSql('ALTER TABLE meeting DROP FOREIGN KEY FK_F515E139BEFD98D1');
        $this->addSql('ALTER TABLE meeting DROP FOREIGN KEY FK_F515E13944F779A2');
        $this->addSql('DROP INDEX IDX_F515E139A76ED395 ON meeting');
        $this->addSql('DROP INDEX IDX_F515E139BEFD98D1 ON meeting');
        $this->addSql('DROP INDEX IDX_F515E13944F779A2 ON meeting');
        $this->addSql('ALTER TABLE meeting DROP user_id, DROP training_id, DROP consultant_id');
    }
}
